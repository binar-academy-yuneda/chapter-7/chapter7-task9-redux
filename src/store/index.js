import { createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";

import { createSlice, configureStore } from "@reduxjs/toolkit";
const initialState = { counter: 0, showCounter: true };
const counterSlice = createSlice({
  name: "counter",
  initialState,
  reducers: {
    increment(state) {
      state.counter++;
    },
    decrement(state) {
      state.counter--;
    },
    increase(state, action) {
      state.counter += action.payload;
    },
    toggleCounter(state) {
      state.showCounter = !state.showCounter;
    },
  },
});

// const store = createStore(counterSlice.reducer, composeWithDevTools());
// const store = createStore(counterReducer, composeWithDevTools());
const store = configureStore({
  reducer: counterSlice.reducer,
});

export const counterActions = counterSlice.actions;
export default store;

// const counterReducer = (state = initialState, action) => {
//     if (action.type === "increment") {
//       return {
//         counter: state.counter + 1,
//         showCounter: state.showCounter,
//       };
//     }

//     if (action.type === "increaseByUserInput") {
//       return {
//         counter: state.counter + action.value,
//         showCounter: state.showCounter,
//       };
//     }

//     if (action.type === "decrement") {
//       return {
//         counter: state.counter - 1,
//         showCounter: state.showCounter,
//       };
//     }
//     if (action.type === "toogleCounter") {
//       return {
//         showCounter: !state.showCounter,
//         counter: state.counter,
//       };
//     }
//     return state;
//   };
